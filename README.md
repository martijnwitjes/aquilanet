# AQUILANET

## Instructions
#### 1 Installation
1. Get the environment from `\\NAS\01_scripts\Libs\py35geomorph.rar` 
2. Choose a 'root directory' where you want to run the application.

#### 2 Preprocessing
AquilaNet needs specially preprocessed data before it can be trained or make predictions.
1. Create a `/sourceData` folder in the 'root directory' where you want to run AquilaNet.
2. Place the .las file that you want to preprocess in the new `/sourceData` folder
3. Invoke `aquila_net.flows.preprocess()` providing the name of the source .las file and the sample size. The function will create a number of new directories; the `/images` directory will contain the preprocessed data, as well as a csv file containing information about each point contained in the sample.

#### 3 Training and optimization
The `aquila_net.flows.compare_models()` function tries out multiple neural network architectures and hyperparameter settings. Each architecture must be specified in `[rootdirectory]/models/[modelname]/make_model.py`. The only hyperparameter that is currently implemented for exploration is a variable that allows for artificially increasing the importance of correctly identifying ground points, the 'class weight'.


For example, a basic model architecture `[rootdirectory]/models/basic/make_model.py` could look like this:

---
```python
x = keras.layers.Conv2D(20, (3, 3), activation='relu')(input_context)

x = keras.layers.Flatten()(x)

x = keras.layers.Dense(40, activation='relu')(x)
```

---
This small python file is found by the `aquila_net.deep_learning.get_model()` function. Once the model is built and validated, it is saved as `[rootdirectory]/models/[modelname]/[modelname].json` and its weights are saved as `[rootdirectory]/models/[modelname]/[modelname]_weights.h5`

#### 4 Predicting and DTM creation
Currently, there is no function to predict on new data with an existing model. This functionality can be adapted from `aquila_net.deep_learning.cross_validate_model()` which outputs a prediction csv file. This csv file can then be transformed into a raster with `aquila_net.raster.csv_to_raster()` 


#### Making a new environment
These notes may save you time when you want to make a new environment for aquilanet.
1. install laspy from github: `pip install git+git://github.com/laspy/laspy@master`
2. install natgrid to enable natural neighbor interpolation in scipy: `pip install git+git://github.com/matplotlib/natgrid@master`

