from datetime import datetime
import os
import pdal
import sys
import shutil
import numpy as np

def get_epsg_from_las(las_file):
    os.system(sys.executable)


def setup(source,sample_size):
    data_dir = os.path.join("Data", str(os.path.split(source)[1].split(".")[0])+"_"+str(sample_size))
    print(data_dir)
    os.makedirs(data_dir,exist_ok=True)
    shutil.copyfile(source,os.path.join(data_dir,"source.las"))
    return data_dir

def report(event,number="latest",location="reports",verbose=False):
    if not os.path.exists(location):
        os.makedirs(location)
    if number == "new":
        number = len(os.listdir(location))+1
    if number == "latest":
        number = len(os.listdir(location))
    report_name = os.path.join(location,"report_{}.csv".format(number))
    if os.path.exists(report_name):
        m = "a"
    else:
        m = "w"
    time = "{}-{}-{},{}:{}".format(datetime.now().day,datetime.now().month,datetime.now().year,datetime.now().hour,datetime.now().minute)
    with open(report_name,m) as f:
        f.write("{},{}{}".format(event,time,"\n"))
    if verbose:
        print("{} at {}:{}".format(event,datetime.now().hour,datetime.now().minute))
    return(number)

def hour_min():
    now = datetime.now()
    return "{}:{}".format(now.hour, now.minute)


def pipe(json, arrays=False, metadata=False, log=False, verbose=False):
    json = json.replace("\\", "/")
    if verbose:
        print(json)
    pipeline = pdal.Pipeline(json)
    pipeline.validate()  # Check if our JSON and options were good
    pipeline.loglevel = 1  # 8 is really noisy
    pipeline.execute()
    arrays = pipeline.arrays if arrays is True else None
    metadata = pipeline.metadata if metadata is True else None
    log = pipeline.log if log is True else None
    sys.stdout.flush()
    return arrays, metadata, log


def cloud_command(command, ccpath=r"C:\Program Files\CloudCompare"):
    old_dir = os.getcwd()
    os.chdir(ccpath)
    os.system(command)
    os.chdir(old_dir)

def shuffle_in_unison(set):
    rng_state = np.random.get_state()
    for arr in set:
        np.random.shuffle(arr)
        np.random.set_state(rng_state)
    return set


def parse_path(path):

    """
    Parses a file or folderpath into: base, folder (where folder is the
    outermost subdirectory), filename, and extension. Filename and extension
    are empty if a directory is passed.
    """

    if path[0] != os.sep:
        path = os.sep + path

    filename = ''

    split_for_ext = path.split('.')
    if len(split_for_ext) > 1:
        extension = '.' + split_for_ext[-1]
    else:
        extension = ''

    # Remove trailing '/'
    path = os.path.normpath(split_for_ext[0])

    if len(extension) > 0:
        filename = path.split(os.sep)[-1]
        path = os.path.join(*path.split(os.sep)[:-1])

    path = list(filter(None,path.split(os.sep)))

    folder = path[-1]
    base = os.path.join(*path[:-1])

    return base, folder, filename, extension