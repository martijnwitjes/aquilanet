from aquila_net.tools import report, pipe, cloud_command
import os
import csv
import multiprocessing as mp
import numpy as np
import laspy
import shutil
from PIL import Image, ImageOps, TiffImagePlugin
TiffImagePlugin.DEBUG = True
import gdal
import tensorflow as tf


def assign_folds(data_dir):
    report("assign_folds")
    folds_dir = os.path.join(data_dir, "folds")
    os.makedirs(folds_dir, exist_ok=True)
    source = os.path.join(data_dir, "source.las")
    capacity = str(int(len(laspy.file.File(source)) / 9))
    json = '''{
                  "pipeline":[
                    "''' + source + '''",
                    {
                      "type":"filters.chipper",
                      "capacity":"''' + capacity + '''"
                    },
                    {
                        "type":"writers.las",
                        "filename":"''' + folds_dir + '''/fold_#.las"
                    }  
                  ]
                }'''
    pipe(json)
    folds = len(os.listdir(os.path.join(data_dir, "folds")))
    print("folds:", folds)
    for fold in range(folds):
        json = '''{
                      "pipeline":[
                        "''' + folds_dir + '''/fold_''' + str(fold + 1) + '''.las",
                        {
                          "type":"filters.assign",
                          "assignment":"PointSourceId[0:9999]=''' + str(fold) + '''"
                        },
                        {
                            "type":"writers.las",
                            "filename":"''' + folds_dir + '''/fold_''' + str(fold + 1) + '''.las"
                        }  
                      ]
                    }'''
        pipe(json)
    json = '''{
                  "pipeline":[
                    "''' + folds_dir + '''/fold_*.las",
                    {
                      "type":"filters.merge"
                    },
                    "''' + data_dir + '''/points.las"
                  ]
                }'''
    pipe(json)
    # for i in os.listdir(folds_dir):
    # os.remove(os.path.join(folds_dir,i))
    # os.removedirs(folds_dir)


def enrich(data_dir,scale):
    report("enrich")
    # Add variables to .las file, save as CSV
    file_name = os.path.join(os.getcwd(),data_dir,"points.las")
    csv_name_renamed = os.path.join(data_dir, "enriched.csv")
    if os.path.isfile(csv_name_renamed):
        print("Detecting existing enriched.csv file; not wasting time on making a new one.")
    else:
        command = "CloudCompare -SILENT -NO_TIMESTAMP -C_EXPORT_FMT asc -EXT csv -ADD_HEADER -AUTO_SAVE OFF -O {0} -ROUGH {1} -CURV NORMAL_CHANGE {1} -DENSITY {1} -SAVE_CLOUDS".format(file_name,scale)
        cloud_command(command)
        csv_name = file_name.split(".")[0] + ".csv"

        # Rename variable names in CSV to enable saving as .las
        with open(csv_name, newline='') as in_file, open(csv_name_renamed, 'w', newline='') as out_file:
            r = csv.reader(in_file,delimiter=" ")
            w = csv.writer(out_file,delimiter=",")
            next(r, None)
            w.writerow(["X", "Y", "Z", "Red", "Green", "Blue", "PointSourceId", "ScanDirectionFlag", "Intensity", "Classification", "Deviation", "Curvature", "Density"]) # Deviation = Roughness; had to rename due to las file format
            for row in r:
                w.writerow(row)
    enriched_las = os.path.join(data_dir, "enriched.las")
    if os.path.isfile(enriched_las):
        print("Detecting existing enriched.las file; not wasting time on making a new one.")
    else:
        # Change renamed csv back to las file

        json = '''{
                          "pipeline":[
                            "''' + csv_name_renamed + '''",
                            {
                                "type":"writers.las",
                                "filename":"''' + enriched_las + '''",
                                "extra_dims":"Density=double,Deviation=float,Curvature=double"
                            }  
                          ]
                        }'''
        pipe(json)


def sample(data_dir, sample_size):
    report("Sampling")
    las_name = os.path.join(os.getcwd(),data_dir,"sample.las")
    enriched_file_name = os.path.join(data_dir,"enriched.las")
    capacity = str(int(len(laspy.file.File(enriched_file_name))/ sample_size))
    json = '''{
              "pipeline":[
                {
                  "type":"readers.las",
                  "filename":"''' + enriched_file_name + '''",
                  "extra_dims":"Density=double,Deviation=float,Curvature=double"
                },
                {
                  "type":"filters.chipper",
                  "capacity":"''' + capacity + '''"
                },
                {
                    "type":"filters.locate",
                    "dimension":"Z",
                    "minmax":"min"
                },
                {
                    "type":"filters.merge"
                },
                {
                    "type":"writers.las",
                    "filename":"''' + las_name + '''",
                    "extra_dims":"Density=double,Deviation=float,Curvature=double"
                }  
              ]
            }'''
    pipe(json)


def crop_one(task):
    i = task[0]

    data_dir = task[1]
    crop_distance = task[2]
    core = int(str(mp.current_process().name).split("-")[1]) - 1
    point_name = "pt_c{}_x{}_y{}_z{}_r{}_g{}_b{}_i{}_ro{}_de{}_cu{}_f{}".format(i[0][5], i[0][0], i[0][1],
                                                                                i[0][2], i[0][10], i[0][11],
                                                                                i[0][12], i[0][3], i[0][13],
                                                                                i[0][14], i[0][15], i[0][8])
    crop_xyz = "{} {} {}".format(i[0][0] / 100, i[0][1] / 100, i[0][2] / 100)
    enriched_path = os.path.split(data_dir)[0] + "/" + os.path.split(data_dir)[1] + "/enriched/" + str(core) + ".las"
    cropped_path = os.path.split(data_dir)[0] +"/"+ os.path.split(data_dir)[1] + "/cropped/" + point_name + ".las"
    json = '''
                        {
                            "pipeline": [
                                {
                                    "type": "readers.las",
                                    "filename": "''' + enriched_path + '''",
                                    "extra_dims":"Density=double,Deviation=float,Curvature=double"
                                },
                                {
                                    "type": "filters.crop",
                                    "point": "POINT (''' + crop_xyz + ''')",
                                    "distance": "''' + str(crop_distance) + '''"
                                },
                                {
                                    "type":"writers.las",
                                    "filename":"''' + cropped_path + '''",
                                    "extra_dims":"Density=double,Deviation=float,Curvature=double"
                                }  
                            ]
                        }'''
    pipe(json)
    return point_name


def crop(data_dir,crop_distance):
    report("crop")
    cpus = mp.cpu_count() - 2
    os.makedirs(os.path.join(data_dir, "enriched"), exist_ok=True)
    os.makedirs(os.path.join(data_dir, "cropped"), exist_ok=True)
    las_file = laspy.file.File(os.path.join(data_dir, "sample.las"))
    sample = las_file.points
    batches = np.array_split(sample,cpus)
    crop_tasks = []

    for core in range(len(batches)):
        if not os.path.isfile(os.path.join(data_dir,"enriched",str(core)+".las")):
            shutil.copyfile(os.path.join(data_dir,"enriched.las"),os.path.join(data_dir,"enriched",str(core)+".las"))
        for point in batches[core]:
            crop_tasks.append([point,data_dir,crop_distance])

    cropped_points = []
    crop_count = 0
    pool = mp.Pool(processes=cpus)
    for point_name in pool.map(crop_one, [task for task in crop_tasks]):
        cropped_points.append(point_name)
        crop_count += 1
        print("cropping:",crop_count,"of",len(crop_tasks))
    return cropped_points


def point_to_png(task):
    point_name = task[0]
    max_size = task[1]
    spatial_reference = task[2]
    data_dir = task[3]
    window_size = str(task[4])
    radius = str(task[5])
    resolution = str(task[6])

    point_path = os.path.join(data_dir,"cropped",point_name)


    os.makedirs(os.path.join(data_dir, "tifs", point_name[:-4]), exist_ok=True)
    os.makedirs(os.path.join(data_dir, "pngs", point_name[:-4]),exist_ok=True)
    for dimension in [["Z",100],["Intensity",100000],["Deviation",5],["Curvature",0.3],["Density",2]]:
        tif_path = os.path.join(data_dir, "tifs", point_name[:-4],dimension[0] + ".tif")
        png_path = os.path.join(data_dir, "pngs", point_name[:-4],dimension[0] + ".png")

        pipe('''{"pipeline":[
        {   "type":"readers.las",
            "filename":"''' + point_path + '''",
            "spatialreference":"''' + spatial_reference + '''",
            "extra_dims":"Density=double,Deviation=float,Curvature=double"
        },
        {   "type":"writers.gdal",
            "filename":"''' + tif_path + '''",
            "gdalopts":"a_srs=''' + spatial_reference + '''",
            "output_type":"mean",
            "window_size":"''' + window_size + '''",
            "radius":"''' + radius + '''",
            "resolution":"''' + resolution + '''",
            "dimension":"''' + dimension[0] + '''"
        }]}''',verbose=False)

        tif = gdal.Open(tif_path)
        gdal.Translate(png_path, tif,options="-of PNG -scale 0 " + str(dimension[1]) + " 0 255")
        with Image.open(png_path) as image:
            expansion = max_size - min(image.size)
            expanded = ImageOps.expand(image, expansion)
            try:
                #print("\t",point_name,dimension,max_size,image.size)
                expanded = ImageOps.fit(expanded, (max_size, max_size))  # je kunt centering aangeven bij deze functie; hoe bepaal je het center bij onvolledige cirkels?
            except:
                print("ERROR",point_name, dimension, max_size, image.size)
            expanded.save(png_path)
    return 0


def points_to_pngs(data_dir,size,spatial_reference,window_size,radius,resolution):
    report("points_to_pngs")
    cpus = mp.cpu_count()-2
    os.makedirs(os.path.join(data_dir,"tifs"),exist_ok=True)
    os.makedirs(os.path.join(data_dir,"pngs"),exist_ok=True)
    points = os.listdir(os.path.join(data_dir,"cropped"))
    tasks = []
    png_count = 0
    for point in points:
        tasks.append([point,size,spatial_reference,data_dir,window_size,radius,resolution])
        png_count += 1
        print("points to pngs:",png_count,"of",len(points))
    pool = mp.Pool(processes=cpus)
    pool.map(point_to_png,tasks)

def png_to_arrays(task):
    png = task[0]
    data_dir = task[1]
    png_path = os.path.join(data_dir,"pngs",png)
    info = png.split("_")
    label = int(info[1][1:])
    if label == 0:
        label = 1
    x = int(info[2][1:])
    y = int(info[3][1:])
    z = int(info[4][1:])
    coords = np.array([x,y,z])
    fold = int(info[12][1:])
    context_arrays = []
    for image in os.listdir(png_path):
        if image.endswith(".png"):
            image_path = os.path.join(png_path,image)
            image = tf.keras.preprocessing.image.load_img(image_path)
            context_array = tf.keras.preprocessing.image.img_to_array(img=image)
            context_array = np.array(context_array,dtype="float")
            context_array = np.mean(context_array,2) / 255
            context_arrays.append(context_array)
    context_arrays = np.array(context_arrays)
    return context_arrays,label,coords,fold

def pngs_to_arrays(data_dir,max=None):
    report("pngs_to_arrays")
    os.makedirs(os.path.join(data_dir,"arrays"),exist_ok=True)
    cpus = mp.cpu_count()-2
    pool = mp.Pool(processes=cpus)
    pngs = os.listdir(os.path.join(data_dir,"pngs"))
    if max is None:
        max = len(pngs)
    tasks = []
    for png in pngs:
        tasks.append([png,data_dir])
    context_data = []
    labels = []
    coords = []
    folds = []
    while True:
        try:
            if max > 1:
                np.random.shuffle(tasks)
                tasks = tasks[0:max]
                for context, label, coord, fold in pool.map(png_to_arrays,[task for task in tasks]):
                    context_data.append(context)
                    labels.append(label)
                    coords.append(coord)
                    folds.append(fold)
                    print("pngs to arrays: ",len(context_data),"of",len(tasks))
                context_data = np.array(context_data)
                labels = np.vstack(labels)
                coords = np.vstack(coords)
                folds = np.vstack(folds)
                print(context_data.shape)
                np.save(arr=context_data, file=os.path.join(data_dir,"arrays","context_data.npy"))
                np.save(arr=labels, file=os.path.join(data_dir,"arrays","labels.npy"))
                np.save(arr=coords, file=os.path.join(data_dir,"arrays","coords.npy"))
                np.save(arr=folds, file=os.path.join(data_dir,"arrays","folds.npy"))
                return 0
            else:
                print(" --- Sample size was reduced to lower than 1; something went very wrong")
                break
        except MemoryError:
            max = int(max * 0.95)
            print(" --- Memory error; reducing sample size by 5% to {}".format(max))








