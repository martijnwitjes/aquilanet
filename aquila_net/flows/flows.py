from aquila_net.tools import *
from aquila_net.deep_learning import *
from aquila_net.preprocessing import *
from aquila_net.raster import *
import math

import numpy as np
import laspy
import os

def preprocess(source,sample_size=100,window_size=2,radius=None,resolution=0.1,size=200,enrich_scale=10,crop_distance=10,spatial_reference="EPSG:32617"):
    '''
    :param source: the location of the training las dataset
    :param sample_size: The number of points that should be converted to context images
    :param window_size:  The maximum distance from a donor cell to a target cell when applying the fallback interpolation method
    :param radius: Radius about cell center bounding points to use to calculate a cell value (default = resolution*sqrt(2))
    :param resolution: Length of raster cell edges in X/Y units
    :param size: Size of the context images
    :param enrich_scale: Radius in which to calculate roughness, density, and curvature
    :param crop_distance: Distance in which to crop around each point
    :param spatial_reference: Code for spatial reference system (e.g.: "EPSG:32617")
    :return:
    '''
    if radius is None:
        radius = resolution * math.sqrt(2)

    report("Started preprocessing", "new")
    data_dir = setup(source,sample_size)
    assign_folds(data_dir)
    enrich(data_dir,enrich_scale)
    sample(data_dir,sample_size)
    crop(data_dir,crop_distance)
    points_to_pngs(data_dir,size,spatial_reference,window_size,radius,resolution)
    pngs_to_arrays(data_dir)
    return data_dir


def optimize_model(data_dir, batch_size, epochs, class_weight_list, epsg=None, model_type=None, folds=10):
    # Create a csv version of the validation sample
    sample_las_path = os.path.join(data_dir,"sample.las")
    sample_csv_path = os.path.join(data_dir,"sample.csv")
    #validation_csv_path = os.path.join(data_dir,"validation","validation_csv.csv")

    pipe('''{
                  "pipeline":[
                    "''' + sample_las_path + '''",
                    {
                        "type":"writers.text",
                        "filename":"''' + sample_csv_path + '''",
                        "quote_header":"false",
                        "precision":"2"
                    }  
                  ]
                }''')

    # Create a 'ground truth' DSM
    report("Making ground truth DSM")
    true_dsm_path = os.path.join(data_dir,"rasters","true_DSM.tif")
    if not os.path.exists(true_dsm_path):
        csv_to_raster(data_dir, sample_csv_path, epsg=epsg, prediction=False, raster_name="true_DSM")

    # Create a 'ground truth' DTM
    report("Making ground truth DTM")
    true_dtm_path = os.path.join(data_dir,"rasters","true_DTM.tif")
    if not os.path.exists(true_dtm_path):
        csv_to_raster(data_dir, sample_csv_path, type="DTM", epsg=epsg, prediction=False, raster_name="true_DTM")

    # Iterate spatial K-fold cross validation over different parameters
    error_paths = []
    for class_weight in class_weight_list:
        model_name = name_model(model_type,class_weight,epochs)
        validation_csv_path = cross_validate_model(data_dir,batch_size,epochs,class_weight,folds,model_type)
        # Transform prediction CSV to DTM
        try:
            predicted_dtm_path = csv_to_raster(data_dir, validation_csv_path, epsg=epsg, raster_name=model_name)
        except RuntimeError:
            print("No ground points predicted. Can't interpolate.")
            predicted_dtm_path = true_dsm_path # This gives the worst possible prediction.
        error_name = str(os.path.split(validation_csv_path)[1].split(".")[0])
        error_path = os.path.join(data_dir,"validation",error_name+"_errors.tif")
        compare_rasters(true_dtm_path, predicted_dtm_path, error_path, epsg=epsg,absolute=True)
        error_paths.append(error_path)
    best_model_stats = compare_errors(error_paths=error_paths)
    train_best_model(best_model_stats[0],data_dir)
    return best_model_stats


def compare_models(model_list, data_dir, batch_size, epochs, class_weight_list, epsg=None, folds=10):
    best_per_model = []
    for model_name in model_list:
        best_model_stats = optimize_model(data_dir, batch_size, epochs,
                                          class_weight_list, epsg, model_name, folds)
        best_per_model.append(best_model_stats)
    means = []
    for stats in best_per_model:
        print("{}\tmean: {:.3g}\tsigma: {:.3g}\tmedian: {:.3g}\tnon-zero: {}".format(stats[0], stats[1], stats[2],stats[3], stats[4]))
        means.append(stats[1])