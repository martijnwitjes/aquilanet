import numpy as np, keras, os
import pandas as pd
from aquila_net.tools import report

def shuffle_in_unison(set):
    rng_state = np.random.get_state()
    for arr in set:
        np.random.shuffle(arr)
        np.random.set_state(rng_state)
    return set

def get_train_valid(fold,data_dir):
    context = np.load(os.path.join(data_dir,"arrays","context_data.npy"))
    labels = np.load(os.path.join(data_dir,"arrays","labels.npy"))-1
    coords = np.load(os.path.join(data_dir,"arrays","coords.npy"))/100
    folds = np.load(os.path.join(data_dir,"arrays","folds.npy"))
    train_idx = np.where(folds != fold)[0]
    valid_idx = np.where(folds == fold)[0]
    context_t = context[train_idx]
    context_v = context[valid_idx]


    labels_t = np.take(labels,train_idx)
    labels_v = np.take(labels,valid_idx)
    ratio_t = sum(labels_t) / len(labels_t)
    ratio_v = sum(labels_v) / len(labels_v)
    print("\ttraining set: {} ground points of {} total ({:.3g})".format(sum(labels_t), len(labels_t),ratio_t))
    print("\tvalidation set: {} ground points of {} total ({:.3g})".format(sum(labels_v), len(labels_v),ratio_v))

    labels_t = keras.utils.to_categorical(labels_t, 2)
    labels_v = keras.utils.to_categorical(labels_v, 2)
    coords_t = np.take(coords,train_idx,axis=0)
    coords_v = np.take(coords,valid_idx,axis=0)
    data = context_t, context_v, labels_t, labels_v, coords_t, coords_v
    return ratio_v, data

def get_all_training_data(data_dir):
    context = np.load(os.path.join(data_dir, "arrays", "context_data.npy"))
    labels = np.load(os.path.join(data_dir, "arrays", "labels.npy")) - 1
    labels = keras.utils.to_categorical(labels, 2)
    coords = np.load(os.path.join(data_dir, "arrays", "coords.npy")) / 100
    data = context, labels, coords
    return data


def get_model(data_dir,model_name="basic"):
    if model_name is None:
        model_name = "basic"
    script_path = os.path.join("models", model_name, "make_model.py")

    # Load data so its shape can be passed to the model input variable
    data = get_all_training_data(data_dir)
    context = data[0]
    input_context_shape = [context.shape[1], context.shape[2], context.shape[3]]
    input_context = keras.layers.Input(shape=input_context_shape)

    # Build model architecture according to the lines in the specified python file
    local_dict = {"input_context": input_context}
    exec(compile(open(script_path).read(), script_path, 'exec'),globals(),local_dict)
    x = local_dict["x"]

    # Finish building the model
    outputs = keras.layers.Dense(2, activation='softmax')(x)
    model = keras.models.Model(inputs=input_context, outputs=outputs)
    model.compile(optimizer='Adadelta', loss="binary_crossentropy", metrics=['accuracy'])
    return model

def validate_model(batch_size,epochs,model,data,class_weight,verbose=1):
    context_t, context_v, labels_t, labels_v = data[0],data[1],data[2],data[3]
    callbacks = [
        keras.callbacks.ReduceLROnPlateau(monitor='loss', factor=0.2, patience=5, min_lr=0.001)]
    model.fit(x=context_t, y=labels_t, class_weight=class_weight, batch_size=batch_size,
                  epochs=epochs,
                  callbacks=callbacks, validation_data=(context_v, labels_v),shuffle=True,verbose=verbose)
    return model, model.evaluate(context_v, labels_v,verbose=verbose)[1]


def train_model(batch_size,epochs,model,data,class_weight):
    context, labels = data[0],data[1]
    callbacks = [
        keras.callbacks.ReduceLROnPlateau(monitor='loss', factor=0.2, patience=5, min_lr=0.001)]
    model.fit(x=context, y=labels, class_weight=class_weight, batch_size=batch_size,
              epochs=epochs,
              callbacks=callbacks, shuffle=True)
    return model


def predict_model(model,data):
    predictions = model.predict(data, batch_size=100)
    predictions = np.argmax(predictions, axis=1)
    return predictions


def kappa(tn,fp,fn,tp):
    """
    :param tn: true negatives (the amount of non-ground points correctly classified as such)
    :param fp: false positives (the amount of non-ground points incorrectly classified as ground points)
    :param fn: false negatives (the amount of ground points incorrectly classified as non-ground points)
    :param tp: true positives (the amount of ground points correctly classified as ground such)
    :return: Cohen's Kappa score (see https://en.wikipedia.org/wiki/Cohen%27s_kappa)
    """
    real_s = tn + tp
    max_s = tn + fp + fn + tp
    random_s = (((tn + fp)*(tn+fn))/max_s) + (((fn + tp)*(fp+tp))/max_s)
    try:
        k = (real_s-random_s)/(max_s-random_s)
    except ZeroDivisionError:
        return 1
    return k


def evaluate_prediction(predictions,truth):
    true_positive = 0
    true_negative = 0
    false_positive = 0
    false_negative = 0
    prediction_types = pd.DataFrame(columns=["Flag"])
    for i in range(len(predictions)):
        if predictions[i] == np.argmax(truth[i]):
            if predictions[i] == 1:
                true_positive += 1
                prediction_types.loc[i] = 1 # true positive
            else:
                true_negative += 1
                prediction_types.loc[i] = 2 # true negative
        else:
            if predictions[i] == 1:
                false_positive += 1
                prediction_types.loc[i] = 3 # false positive
            else:
                false_negative += 1
                prediction_types.loc[i] = 4 # false negative
    try:
        true_positive_rate = true_positive / (true_positive + false_negative)
    except ZeroDivisionError:
        true_positive_rate = 0
    try:
        false_positive_rate = false_positive / (true_negative + false_positive)
    except ZeroDivisionError:
        false_positive_rate = 0
    k = kappa(true_negative, false_positive, false_negative, true_positive)
    accuracy = (true_positive + true_negative) / (true_positive + true_negative + false_positive + false_negative)
    print("\tAccuracy: {:.3g}"
          "\n\tKappa: {:.3g}"
          "\n\tTrue Positive Rate: {:.3g}"
          "\n\tFalse Positive Rate: {:.3g}"
          "\n\tTrue Positives: {:.3g}"
          "\n\tTrue Negatives: {:.3g}"
          "\n\tFalse Positives: {:.3g}"
          "\n\tFalse Negatives: {:.3g}".format(accuracy,k,true_positive_rate,false_positive_rate,
                                true_positive,true_negative,false_positive,false_negative))
    return prediction_types


def cross_validate_model(data_dir,batch_size,epochs,class_weight,folds=10,model_type="basic"):
    model_name = name_model(model_type,class_weight,epochs)
    os.makedirs(os.path.join(data_dir, "validation"), exist_ok=True)
    accs = []
    ratios = []
    results = pd.DataFrame(columns=["X","Y","Z", # = coordinates
                                    "ClassFlags", # = predicted class
                                    "Classification", # = true class
                                    "Flag"]) # = true positive / false positive / true negative / false negative
    for i in range(folds):
        print("\nFOLD", i, "\t\tmodel:", model_name)
        ratio, data = get_train_valid(i,data_dir)
        model = get_model(data_dir,model_type)
        model_i, acc = validate_model(batch_size,epochs,model,data,class_weight)
        accs.append(acc)
        ratios.append(ratio)
        fold_preds = predict_model(model_i,data[1])
        truth = pd.DataFrame(np.argmax(data[3],axis=1),columns=["Classification"])
        coords = pd.DataFrame(data[5],columns=["X","Y","Z"])
        types = evaluate_prediction(fold_preds, data[3])
        fold_preds = pd.DataFrame(fold_preds, columns=["ClassFlags"])
        fold_results = pd.concat([coords,fold_preds,truth,types],axis=1)
        results = results.append(fold_results)

    validation_csv_path = os.path.join(data_dir,"validation","validation_{}.csv".format(model_name))

    pd.DataFrame.to_csv(results,validation_csv_path,index=False)

    print("\nSUMMARY","model:\t\t",model_name)
    for i in range(len(ratios)):
        print("\tFold {:.3g}\tAccuracy: {:.3g}\tRatio: {:.3g}".format(i,accs[i],ratios[i],))
    print("AVERAGE:\tAccuracy: {:.3g}\tRatio: {:.3g}".format(np.average(accs),np.average(ratios)))
    return validation_csv_path


def parse_model_name(model_name):
    model_stats = model_name.replace("-", ".")
    model_type = model_stats.split("_")[0]
    weights = model_stats.split("_")[1:3]
    epochs = model_name.split("_")[3]
    class_weight = {0: weights[0], 1: weights[1]}
    return model_type, class_weight, epochs


def name_model(model_type, class_weight, epochs):
    class_weight_string = str(class_weight[0]).replace(".", "-") + "_" + str(class_weight[1]).replace(".", "-")
    model_string = "{}_{}_{}".format(model_type,class_weight_string,epochs)
    return model_string


def train_best_model(model_name,data_dir):
    report("Training best model: " + model_name)
    model_name, class_weight, epochs = parse_model_name(model_name)
    data = get_all_training_data(data_dir)
    model = get_model(data_dir,model_name)
    model = train_model(batch_size=100,
                epochs=int(epochs),
                model=model,
                data=data,
                class_weight=class_weight)
    weights_path = os.path.join("models",model_name,model_name + "_weights.h5")
    model_path = os.path.join("models",model_name,model_name + ".json")
    model.save_weights(weights_path)
    with open(model_path,'w') as f:
        f.write(model.to_json())