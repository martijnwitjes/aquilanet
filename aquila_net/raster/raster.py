import gdal
import laspy
import numpy as np
import os
import sys
from osgeo.gdalconst import *
from aquila_net.tools import report, pipe, parse_path
from subprocess import DEVNULL, STDOUT, check_call
import osr, ogr
from matplotlib.mlab import griddata

import warnings
warnings.filterwarnings("ignore",".*griddata function was deprecated.*")


gdal.UseExceptions()

def csv_to_raster(data_dir, csv_path, epsg, resolution=1, prediction=True, separator=",", raster_name=None, type="dtm"):
    report("Started converting points to {}".format(type),number="new")
    os.makedirs(os.path.join(data_dir,"rasters"),exist_ok=True)
    if not csv_path.endswith(".csv"):
        csv_path = csv_path + ".csv"
    if raster_name is None:
        file_path = os.path.join(data_dir,"rasters",os.path.split(csv_path)[1].split(".")[0])
    else:
        file_path = os.path.join(data_dir,"rasters",raster_name)
    filtered_csv_path = file_path + "_filtered.csv"
    reference_tif_path = file_path +"_reference.tif"
    filtered_las_path = file_path + "_filtered.las"
    interpolated_tif_path = os.path.join(data_dir,"rasters",os.path.split(file_path)[1].split(".")[0] + ".tif")
    if type == "dtm" or type == "DTM":
        # Select predicted ground points
        if prediction:
            limits = "ClassFlags[1:1]"
        else:
            limits = "Classification[1:1]"
    else:
        limits = "Classification[0:100]"
    pipe('''{
            "pipeline":[
                {
                    "type": "readers.text",
                    "filename": "''' + csv_path + '''",
                    "separator":"''' + separator + '''"       
                },
                {
                    "type":"filters.range",
                    "limits":"''' + limits + '''"
                },
                {
                    "type":"writers.text",
                    "filename":"''' + filtered_csv_path + '''",
                    "quote_header":"false"
                }
            ]
        }''')

    # Create reference raster
    pipe('''{
            "pipeline": [
                {
                    "type": "readers.text",
                    "filename":"''' + filtered_csv_path + '''",
                    "separator":","
                  
                },
                {
                    "type": "writers.gdal",
                    "filename": "''' + reference_tif_path + '''",
                    "gdaldriver":"GTiff",
                    "dimension":"Z",
                    "window_size":"1",
                    "radius":"1",
                    "resolution":"''' + str(resolution) + '''"
                }
            ]
        }''')
    # Get raster shape etc. from reference raster
    reference = gdal.Open(reference_tif_path)
    rows = reference.RasterYSize
    cols = reference.RasterXSize

    # Create .las version of filtered dataset for further processing
    try:
        pipe('''{
                    "pipeline":[
                        {
                            "type": "readers.text",
                            "filename": "''' + filtered_csv_path + '''"                    
                        },
                        {
                            "type":"filters.range",
                            "limits":"''' + limits + '''"
                        },
                        {
                            "type":"writers.las",
                            "filename":"''' + filtered_las_path + '''",
                            "extra_dims":"all"
                            
                        }
                    ]
                }''')
    except RuntimeError:
        report("RuntimeError: No points found to rasterize")

    # Convert .las version to numpy arrays
    las_file = laspy.file.File(filtered_las_path, mode="r")
    x_1 = np.asarray(las_file.x)
    y_1 = np.asarray(las_file.y)
    z_1 = np.asarray(las_file.z)
    x = []
    y = []
    z = []
    duplicates = 0
    for i in range(len(z_1)):
        duplicate = False
        for j in range(i + 1, len(z_1)):
            if x_1[i] == x_1[j] and y_1[i] == y_1[j]:
                duplicate = True
                duplicates += 1
        if duplicate is False:
            x.append(x_1[i])
            y.append(y_1[i])
            z.append(z_1[i])
    if duplicates > 0:
        report("{} duplicates removed".format(duplicates))
    las_file.close()
    # Perform natural neighbour interpolation on numpy arrays
    report("Interpolating")
    reference = reference.GetRasterBand(1).ReadAsArray()
    zeros = np.zeros(reference.shape)
    try:
        xi = np.linspace(min(x), max(x), cols)
        yi = np.linspace(min(y), max(y), rows)
        interpolated = griddata(x, y, z, xi, yi)
        interpolated = np.asarray(interpolated)
    except RuntimeError:
        report("RuntimeError: No points found to interpolate")
        interpolated = zeros
    except ValueError:
        print("ValueError: No points found to interpolate")
        interpolated = zeros
    zeros = np.nan_to_num(zeros)
    interpolated = np.nan_to_num(interpolated)
    interpolated = zeros + interpolated


    # Create the raster
    report("Rasterizing")
    gdal.AllRegister()
    driver = gdal.GetDriverByName('GTiff')
    outRaster = driver.Create(interpolated_tif_path, cols, rows, 1, GDT_Float32)
    outBand = outRaster.GetRasterBand(1)
    outBand.WriteArray(interpolated)

    # georeference the image and set the epsg
    outRaster.SetGeoTransform((min(x), 1, 0, min(y), 0, 1))
    srs = osr.SpatialReference()
    srs.ImportFromEPSG(epsg)
    outRaster.SetProjection(srs.ExportToWkt())

    # Flush data to disk, set the NoData value and calculate stats
    outBand.FlushCache()
    outRaster.FlushCache()
    outBand.SetNoDataValue(0)
    del outRaster
    del outBand
    os.remove(filtered_las_path)
    os.remove(filtered_csv_path)
    os.remove(reference_tif_path)
    return interpolated_tif_path


def flip_raster(source_path,flipped_path):
    '''
    This function flips a raster while retaining correct georeference.
    :param source_path: filepath of the raster that should be flipped
    :param flipped_path: filepath of the new flipped raster
    :return:
    '''
    source = gdal.Open(source_path)
    projection = source.GetProjection()
    geotransform = source.GetGeoTransform()
    x_offset = geotransform[2]
    x_size = source.RasterXSize
    y_size = source.RasterYSize
    gdal.AllRegister()
    driver = gdal.GetDriverByName('GTiff')

    flipped = driver.Create(flipped_path, source.RasterXSize,source.RasterYSize, 1, gdal.GDT_Float32)
    flipped.SetProjection(projection)
    flipped.SetGeoTransform(geotransform)

    source_band = source.GetRasterBand(1)
    flipped_band = flipped.GetRasterBand(1)

    for line in range(y_size):
        if line - y_size < 0:
            data_src = source_band.ReadAsArray(x_offset, line, x_size, 1, x_size, 1)
            flipped_band.WriteArray(data_src, x_offset, abs(line - y_size) - 1)
    return 0


def compare_rasters(raster_1_path, raster_2_path, comparison_path,epsg,output_type=None,absolute=False):
    '''
    Overlays two rasters and subtracts the second raster from the first raster.
    :param raster_1_path: The extent of this raster will be used to clip the other raster
    :param raster_2_path: If there is a size difference, this raster should be the bigger one of the two; it is clipped to the boundaries of the other raster.
    :param comparison_path: The path where the comparison raster dataset should be stored
    :return: a geotiff with three bands: The two original rasters (clipped to the size of raster 2) and the result of the subtraction.
    '''
    # Open rasters
    raster_1 = gdal.Open(raster_1_path)
    raster_2 = gdal.Open(raster_2_path)
    projection_wkt = raster_1.GetProjection()

    # Check which raster is smallest
    if (raster_1.RasterXSize*raster_1.RasterYSize) < (raster_2.RasterXSize*raster_2.RasterYSize):
        smallest_raster = raster_1
        smallest_raster_path = raster_1_path
        biggest_raster = raster_2
        biggest_raster_path = raster_2_path
        polygon_path = raster_1_path.split(".")[0] + "_cutline.shp"
        smallest = 1
    else:
        smallest_raster = raster_2
        smallest_raster_path = raster_2_path
        biggest_raster = raster_1
        biggest_raster_path = raster_1_path
        polygon_path = raster_2_path.split(".")[0] + "_cutline.shp"
        smallest = 2
    biggest_raster_warped_path = biggest_raster_path.split(".")[0]+"_warped.tif"
    smallest_raster_warped_path = smallest_raster_path.split(".")[0] + "_warped.tif"
    #polygonize_smallest raster
    #   Make shapefile
    shp_driver = ogr.GetDriverByName("ESRI Shapefile")
    if os.path.exists(polygon_path):
        os.remove(polygon_path)
    shapefile = shp_driver.CreateDataSource(polygon_path)

    #   Get projection from raster
    srs = osr.SpatialReference()
    srs.ImportFromWkt(smallest_raster.GetProjectionRef())

    #   Create layer with projection
    layer = shapefile.CreateLayer(polygon_path, srs=srs)
    min_x, max_x, min_y, max_y = layer.GetExtent()
    mask_band_path = smallest_raster_path.split(".")[0] + "_mask.tif"


    #   Write the band into the layer.
    gdal_calc = os.path.join(os.path.split(sys.executable)[0],"scripts","gdal_calc.py")
    check_call('{} {} -A {} --outfile={} --calc="(A>0)/(A>0)"'.format(sys.executable,gdal_calc,smallest_raster_path,mask_band_path), stdout=DEVNULL, stderr=STDOUT)
    mask_raster = gdal.Open(mask_band_path)
    mask_band = mask_raster.GetRasterBand(1)
    gdal.Polygonize(srcBand=mask_band,maskBand=mask_band,outLayer=layer,iPixValField=-1,options=[],callback=None)
    shapefile = None
    mask_band = None
    mask_raster = None

    x_res = smallest_raster.RasterXSize
    y_res = smallest_raster.RasterYSize

    output_bounds = (min_x,min_y,max_x,max_y)
    biggest_raster_warped = gdal.Warp(
        destNameOrDestDS=biggest_raster_warped_path,
        srcDSOrSrcDSTab=biggest_raster,
        outputBounds=output_bounds,
        dstSRS="EPSG:"+str(epsg),
        cutlineDSName=polygon_path,
        cropToCutline=True,
        width=x_res,
        height=y_res)

    smallest_raster_warped = gdal.Warp(
        destNameOrDestDS=smallest_raster_warped_path,
        srcDSOrSrcDSTab=smallest_raster,
        dstSRS="EPSG:" + str(epsg),
        cutlineDSName=polygon_path,
        cropToCutline=True,
        width=x_res,
        height=y_res)

    #biggest_raster_warped = gdal.Open(biggest_raster_warped_path)
    biggest_raster_array = biggest_raster_warped.GetRasterBand(1).ReadAsArray()
    del biggest_raster_warped
    smallest_raster_array = smallest_raster_warped.GetRasterBand(1).ReadAsArray()
    del smallest_raster_warped

    # Subtract raster 2 from raster 1 and write to a new raster file
    if smallest == 1:
        difference = biggest_raster_array - smallest_raster_array
    else:
        difference = smallest_raster_array - biggest_raster_array
    if absolute:
        difference = np.abs(difference)

    # Somewhat messy way of dealing with some bugs ##################################################
    # First, remove outliers created by the imperfect overlap of the two arrays during subtraction.
    if output_type == "errors":
        std_limit = 8
    else:
        std_limit = 4
    difference[np.abs(difference) > (np.std(difference)*std_limit)] = 0
    # Second, replace any 0-value entries with NODATA
    difference[difference == 0] = np.nan
    # Third, replace any negative values with 0 if making an object height raster
    if output_type == "object height":
        difference[difference < 0] = 0

    rows,cols = difference.shape
    gdal.AllRegister()
    driver = gdal.GetDriverByName('GTiff')
    temp_path = comparison_path.split(".")[0]+"_temp.tif"
    out_raster = driver.Create(temp_path, cols, rows, 1, gdal.GDT_Float32)
    difference_band = out_raster.GetRasterBand(1)
    difference_band.WriteArray(difference)
    out_raster.SetProjection(projection_wkt)
    out_raster.SetGeoTransform(raster_1.GetGeoTransform())
    out_raster = None

    os.remove(biggest_raster_warped_path)
    os.remove(smallest_raster_warped_path)
    os.remove(mask_band_path)
    for ext in [".dbf",".shp",".shx",".prj"]:
       os.remove(polygon_path.split(".")[0]+ext)
    flip_raster(temp_path,comparison_path)
    os.remove(temp_path)


def compare_errors(error_paths=None,validation_dir=None):
    report("compare_errors")
    print("")
    if error_paths is None:
        error_paths = []
        paths = os.listdir(validation_dir)
        for path in paths:
            if path.endswith("_errors.tif"):
                error_paths.append(os.path.join(validation_dir,path))
    all_stats = []
    means = [1000]
    best = None
    for error_path in error_paths:
        error_raster = gdal.Open(error_path)
        errors = error_raster.GetRasterBand(1).ReadAsArray()
        errors = np.abs(errors)
        model_type = parse_path(error_path)[2].replace("_errors", "").replace("validation_","")
        stats = model_type, np.nanmean(errors), np.nanstd(errors), np.nanmedian(errors),np.count_nonzero(errors)
        all_stats.append(stats)
        means.append(np.nanmean(errors))
        if np.nanmean(errors) <= min(means):
            best = stats
        print("{}\tmean: {:.3g}\tsigma: {:.3g}\tmedian: {:.3g}\tnon-zero: {}".format(stats[0],stats[1],stats[2],stats[3],stats[4]))
    return best

