"""Setup for AquilaNet"""
import setuptools

setuptools.setup(
    name='aquila_net',
    version='0.1',
    description="Everything you need to make DTMs with deep learning!",
    author="Martijn Witjes",
    author_email="martijnwitjes@gmail.com",
    url="https://bitbucket.org/martijnwitjes/aquilanet",
    packages=setuptools.find_packages()
)

